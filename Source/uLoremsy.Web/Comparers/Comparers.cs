﻿namespace uTagsy.Web.Comparers
{
    using System.Collections.Generic;

    using Umbraco.Core.Models;

    /// <summary>
    /// Comparer for post dates.
    /// </summary>
    public class IContentComparer : IComparer<IContent>
    {
        public int Compare(IContent x, IContent y)
        {
            if (x.Id < y.Id) { return -1; }
            if (x.Id == y.Id) { return 0; }

            return 1;
        }
    }
}
