﻿namespace uTagsy.Web.Helpers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Collections;

    using Umbraco.Core.Models;
    using Umbraco.Web;

    public class IContentHelper
    {
        public static void SortNodes(int parentId, IEnumerable<IContent> IContents, IComparer<IContent> comparer)
        {
            var list = IContents.ToList();
            list.Sort(comparer);

            var nodeSorter = new umbraco.presentation.webservices.nodeSorter();
            var sortOrder = string.Empty;
            foreach (var doc in list)
            {
                sortOrder += doc.Id + ",";
            }

            nodeSorter.UpdateSortOrder(parentId, sortOrder.TrimEnd(','));
        }





        /// <summary>
        ///  Searches up the tree until it hits rootAlias, then does a breadth first search to get a IContent by it's alias.
        /// </summary>
        /// <param name="current"></param>
        /// <param name="rootAlias"></param>
        /// <param name="alias"></param>
        /// <returns></returns>
        public static IContent GetIContentByAlias(IContent current, string rootAlias, string alias)
        {
            var contentService = UmbracoContext.Current.Application.Services.ContentService;
            var doc = current;

            // get uBlogsyLanding
            while (doc.ContentType.Alias != rootAlias)
            {
                if (doc.ParentId == -1)
                {
                    break;
                }

                doc = contentService.GetById(doc.ParentId);
            }

            // now do breathfirst search
            var q = new Queue();
            q.Enqueue(doc);
            foreach (var d in contentService.GetChildren(doc.Id))
            {
                if (d.ContentType.Alias == alias)
                {
                    // required node is at the top level so return it!
                    return d;
                }

                // Queue the doc because the node with alias == alias is deeper
                q.Enqueue(d);
            }

            // process the queue    
            while (q.Count > 0)
            {
                var d = (IContent)q.Dequeue();
                if (d.ContentType.Alias == alias)
                {
                    // found the node so return!
                    return d;
                }

                foreach (var child in contentService.GetChildren(d.Id))
                {
                    if (child.ContentType.Alias == alias)
                    {
                        // found the node so return!
                        return child;
                    }
                    q.Enqueue(child);
                }
            }

            // no node found :(
            return null;
        }



        /// <summary>
        ///  Returns a value from the ancestor specified by ancestorAlias.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static string GetValueFromAncestor(IContent doc, string ancestorAlias, string propertyAlias)
        {
            var node = GetIContentByAlias(doc, ancestorAlias, ancestorAlias);

            return node.GetValue(propertyAlias).ToString();
        }



        /// <summary>
        /// Creates a content node.
        /// </summary>
        /// <param name="nodeName">The node name</param>
        /// <param name="nodeTypeAlias">The nodeTypeAlias</param>
        /// <param name="properties">A dictionary of node properties</param>
        /// <param name="parentId">The parent node</param>
        /// <param name="publish">Publish or just save?</param>
        /// <returns>IContent</returns>
        public static IContent CreateContentNode(string nodeName, string nodeTypeAlias, Dictionary<string, object> properties, int parentId, bool publish)
        {
            var contentService = UmbracoContext.Current.Application.Services.ContentService;

            // create new content node
            var doc = contentService.CreateContent(nodeName, parentId, nodeTypeAlias);

            // load properties and saves
            return UpdateContentNode(doc, properties, publish);
        }







        /// <summary>
        /// Updates properties in a content node.
        /// </summary>
        /// <param name="doc">The IContent</param>
        /// <param name="properties">A dictionary of node properties</param>
        /// <param name="publish">Publish or just save?</param>
        /// <returns></returns>
        public static IContent UpdateContentNode(IContent doc, Dictionary<string, object> properties, bool publish)
        {
            var contentService = UmbracoContext.Current.Application.Services.ContentService;
            
            // save properties
            doc = UpdateContentProperties(doc, properties);

            // publish the node
            if (publish)
            {
                contentService.SaveAndPublish(doc, raiseEvents: true);
            }

            return doc;
        }



        /// <summary>
        /// Iterates over properties and loads them into the document properties. Calls Save() without raising events.
        /// </summary>
        /// <param name="doc">The document</param>
        /// <param name="properties">A dictionary of properties</param>
        /// <returns>The updated Document</returns>
        public static IContent UpdateContentProperties(IContent doc, Dictionary<string, object> properties)
        {
            var contentService = UmbracoContext.Current.Application.Services.ContentService;

            foreach (var key in properties.Keys)
            {
                doc.SetValue(key, properties[key]);
            }

            contentService.Save(doc, raiseEvents: false);
            return doc;
        }




        /// <summary>
        /// Creates node if it does not exist.
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="doc"></param>
        /// <param name="alias"></param>
        /// <param name="name"></param>
        /// <param name="publish"></param>
        /// <returns></returns>
        public static IContent EnsureNodeExists(int parentId, IContent doc, string alias, string name, bool publish)
        {
            return doc ?? CreateContentNode(name, alias, new Dictionary<string, object>(), parentId, publish);
        }

    }
}
