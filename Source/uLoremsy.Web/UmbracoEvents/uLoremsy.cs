﻿using Umbraco.Core.Logging;

namespace uLoremsy.Web.UmbracoEvents
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Xml.Linq;
    using Umbraco.Core;
    using Umbraco.Core.Events;
    using Umbraco.Core.Models;
    using Umbraco.Core.Services;

    using File = System.IO.File;

    public class uLoremsy : ApplicationEventHandler
    {
        protected const string configFile = "~/config/uLoremsy.config";

        public uLoremsy()
        {
        }


        protected override void ApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            base.ApplicationInitialized(umbracoApplication, applicationContext);
            ContentService.Created += this.ContentService_Created;
        }

        //public void OnApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        //{
        //    //ContentService.Creating += ContentService_Creating;
           // ContentService.Created += this.ContentService_Created;
        //}


        /// <summary>
        /// On new document, prepopulate.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ContentService_Created(IContentService sender, NewEventArgs<IContent> e)
        {
            // read xml file
            string fileContent = File.ReadAllText(HttpContext.Current.Server.MapPath(configFile));

            // get trees
            var trees = XDocument.Parse(fileContent).FirstNode.Document.Descendants("tree");

            // get wildcard tree
            var wildcardTreeXml = this.GetuLoremsyTreeWildcard(trees);
            if (wildcardTreeXml != null)
            {
                this.ProcessTree(sender, e.Entity, wildcardTreeXml);
            }

            // process specified tree and properties - overwrites
            var treeXml = this.GetuLoremsyTreeXElem(sender, e.Entity, trees);
            if (treeXml != null)
            {
                var properties = this.ProcessTree(sender, e.Entity, treeXml);
            }
        }

        public void OnApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
        }

        public void OnApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
        }




        private IContent ProcessTree(IContentService contentService, IContent document, XElement treeXml)
        {
            // do wildcard doctype first
            var wildcardDoctype = this.GetuLoremsyDocTypeWildcard(treeXml);

            if (wildcardDoctype != null)
            {
                // get properties from uLoremsy.config
                var wildcardProps = wildcardDoctype.Descendants("property").Where(x => x.Attribute("enabled").Value == "true");

                // process prevalues
                this.SetPreValues(contentService, document, wildcardProps);
            }

            // get specified doctype
            var doctypeDefs = this.GetuLoremsyDoctypeDefsXElem(document, treeXml);
            foreach (var docType in doctypeDefs)
            {
                // get properties from uLoremsy.config
                IEnumerable<XElement> properties = docType.Descendants("property").Where(x => x.Attribute("enabled").Value == "true");

                // process prevalues
                this.SetPreValues(contentService, document, properties);
            }

            return document;
        }



        /// <summary>
        /// Iterate over xml, set values in document.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="propertiesXml"></param>
        /// <returns></returns>
        private IContent SetPreValues(IContentService contentService, IContent document, IEnumerable<XElement> propertiesXml)
        {
            // iterate over properties and set values
            foreach (var property in propertiesXml)
            {
                var propertyAlias = property.Attribute("alias").Value;
                var value = property.Value.Trim();

                value = this.GetFromStandardValues(contentService, document, value);

                // check for existance of property
                if (!document.HasProperty(propertyAlias)) { continue; }


                // if some other event handler (eg. uDateFoldersy) has already set the value, just skip.
                if (document.GetValue(propertyAlias) == null)
                {
                    // set value
                    document.SetValue(propertyAlias, value);
                }
                else if (document.GetValue(propertyAlias).ToString() == string.Empty)
                {
                    // set value
                    document.SetValue(propertyAlias, value);
                }
            }

            contentService.Save(document);
            return document;
        }



        /// <summary>
        /// Gets value from supported .Net and Umbraco properties.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="xmlValue"></param>
        /// <returns></returns>
        private string GetFromStandardValues(IContentService contentService, IContent document, string xmlValue)
        {
            string value = xmlValue;

            switch (xmlValue)
            {
                case "DateTime.Now":
                    value = DateTime.Now.ToString();
                    break;
                case "DateTime.UtcNow":
                    value = DateTime.UtcNow.ToString();
                    break;
                case "Document.Id":
                    value = document.Id.ToString();
                    break;
                case "Document.DateCreated":
                    value = document.CreateDate.ToString();
                    break;
                case "Document.DateUpdated":
                    value = document.UpdateDate.ToString();
                    break;
                case "Document.Name":
                    value = document.Name;
                    break;
                case "Document.NodeTypeAlias":
                    value = document.ContentType.Alias;
                    break;
                default:
                    if (xmlValue.StartsWith("Document.Properties."))
                    {
                        // get value from custom property
                        var alias = xmlValue.Replace("Document.Properties.", string.Empty);
                        if (!document.HasProperty(alias)) { return string.Empty; }

                        value = document.GetValue(alias).ToString();
                    }
                    else if (xmlValue.StartsWith("Document.Parent."))
                    {
                        // recursively go up parent tree until value found
                        value = this.GetFromStandardValues(contentService, contentService.GetById(document.ParentId), xmlValue.Replace("Document.Parent", "Document"));
                    }
                    break;
            }
            return value;
        }


        /// <summary>
        /// Get doctype from uLoremsy.config which mataches current doctype
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="tree"></param>
        /// <returns></returns>
        private IEnumerable<XElement> GetuLoremsyDoctypeDefsXElem(IContent document, XElement tree)
        {
            var docTypeDefs = tree.Descendants("documentType")
                    .Where(x => x.Attribute("nodeTypeAlias").Value
                                    .Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                                    .Select(s => s.Trim())
                                    .Contains(document.ContentType.Alias))

                    .Where(x => x.Attribute("enabled").Value == "true");

            return docTypeDefs;
        }


        /// <summary>
        /// Get wildcard doctype
        /// </summary>
        /// <param name="tree"></param>
        /// <returns></returns>
        private XElement GetuLoremsyDocTypeWildcard(XElement tree)
        {
            return tree.Descendants("documentType")
                .Where(x => x.Attribute("nodeTypeAlias").Value == "*")
                .SingleOrDefault(x => x.Attribute("enabled").Value == "true");
        }


        /// <summary>
        /// Get tree from uLoremsy.config which has same node name as sender's root.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="trees"></param>
        /// <returns></returns>
        private XElement GetuLoremsyTreeXElem(IContentService contentService, IContent document, IEnumerable<XElement> trees)
        {
            try
            {
                var rootNode = GetRoot(contentService, document);


                // get tree from uLoremsy.config which matches root
                var tree = trees.Where(x => x.Attribute("nodeName").Value == rootNode.Name)
                    .SingleOrDefault(x => x.Attribute("enabled").Value == "true");

                return tree;
            }
            catch (Exception ex)
            {
                LogHelper.Error<uLoremsy>("uLoremsy error.", ex);
                return null;
            }
        }



        /// <summary>
        /// Get wildcard tree.
        /// </summary>
        /// <param name="trees"></param>
        /// <returns></returns>
        private XElement GetuLoremsyTreeWildcard(IEnumerable<XElement> trees)
        {
            return trees.Where(x => x.Attribute("nodeName").Value == "*")
                        .SingleOrDefault(x => x.Attribute("enabled").Value == "true");
        }





        /// <summary>
        /// Gets root of tree
        /// </summary>
        /// <param name="sender"></param>
        /// <returns></returns>
        private IContent GetRoot(IContentService contentService, IContent sender)
        {
            var current = sender;
            while (current.ParentId != -1)
            {
                current = contentService.GetById(current.ParentId);
            }

            return current;
        }


    }
}